cmake_minimum_required(VERSION 3.2)
project(Algorithms C)

set(CMAKE_C_STANDARD 11)

add_library(classicalgor SHARED classics.c)