#include <stdio.h>


/** HASHING**/
static const unsigned MOD_ADLER = 65521;

unsigned adler32(const unsigned char *data, size_t len) {
    unsigned a = 1, b = 0;
    size_t index;

    for (index = 0; index < len; ++index) {
        a = (a + data[index]) % MOD_ADLER;
        b = (b + a) % MOD_ADLER;
    }

    return (b << 16) | a;
}

/** END HASHING**/


/** SORTING **/
static void swap(float *i, float *y) {
    float z = *i;
    *i = *y;
    *y = z;
}

void quick_sort(float *numberList, long low, long high) {
    if (low >= high)
        return;

    long pivot = low;
    long last = pivot;
    long i;

    for (i = pivot + 1; i <= high; i++) {
        if (numberList[i] <= numberList[pivot]) {
            last++;
            swap(&numberList[last], &numberList[i]);
        }
    }

    swap(&numberList[last], &numberList[pivot]);

    quick_sort(numberList, low, last - 1);
    quick_sort(numberList, last + 1, high);
}

/** END SORTING **/


/** SEARCHING **/
int binary_search(float *arr, int start, int end, float search) {
    int half = start + ((end - start) / 2);
    if (start > end) {
        return -1;
    }
    if (arr[half] == search) {
        return half;
    }

    if (arr[half] > search) {
        return binary_search(arr, start, half - 1, search);
    } else {
        return binary_search(arr, half + 1, end, search);
    }
}


int binary_search_iter(const float *arr, int arrLen, float search) {
    int start = 0, end = arrLen;
    while (start <= end) {
        int middle = start + ((end - start) / 2);
        if (arr[middle] > search) {
            end = middle - 1;
        } else if (arr[middle] < search) {
            start = middle + 1;
        } else {
            return middle;
        }
    }
    return -1;
}

/** SEARCHING **/