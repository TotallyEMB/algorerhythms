//
// Created by Yevgeny on 6/24/2018.
//

#ifndef ALGORERHYTHMS_CLASSICS_H
#define ALGORERHYTHMS_CLASSICS_H
#ifdef __cplusplus
extern "C" {
#endif

unsigned adler32(const unsigned char *data, size_t len);
void quick_sort(float *numberList, long low, long high);
int binary_search(float *arr, int start, int end, float search);
int binary_search_iter(const float *arr, int arrLen, float search);

#ifdef __cplusplus
}
#endif
#endif //ALGORERHYTHMS_CLASSICS_H
