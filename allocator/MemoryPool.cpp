//
// Created by Yevgeny on 2/17/2017.
//

#include "MemoryPool.h"

void *MemoryPool::allocate() {
    void *result = (void *) (0);
    for (int i = 0; i < 4096 / 32; i++) {
        Block *iter = (Block *) POOL + i * 32;
        if (isFree(iter->meta)) {
            iter->meta = 1 & 0x0;
            result = iter + sizeof(Block);
            break;
        }
    }

    return result;
}

void MemoryPool::free(void *ptr) {
    Block *block = (Block *) ptr - sizeof(Block);
    block->meta = 1 & 0x1;
}

void MemoryPool::print() {
    int numFreeBlocks = 0;
    std::string draw;
    for (int i = 0; i < 128; i++) {
        Block *iter = (Block *) POOL + i * 32;
        if (isFree(iter->meta)) {
            numFreeBlocks++;
            draw += "[    ]";
        } else {
            draw += "[used]";
        }
    }
    std::cout << "Free Blocks " << numFreeBlocks << '\n' << draw << '\n';
}

unsigned int MemoryPool::left() {
    return 0;
}

MemoryPool::MemoryPool() {
    for (int i = 0; i < 4096 / 32; i++) {
        Block *block = (Block *) POOL + i * 32;
        block->meta = 1 & 0x1;
    }
}