//
// Created by Yevgeny on 2/17/2017.
//

#ifndef ALGORERHYTHMS_MEMORYPOOL_H
#define ALGORERHYTHMS_MEMORYPOOL_H
#include <iostream>

class MemoryPool {
public:
    MemoryPool();
    void *allocate();
    void free(void *ptr);
    void print();
    unsigned int left();
private:
    char POOL[4096];
    struct Block {
        char meta;
    };

    inline bool isFree(char val) {
        return (val & 0x1) != 0;
    }
};


#endif //ALGORERHYTHMS_MEMORYPOOL_H
