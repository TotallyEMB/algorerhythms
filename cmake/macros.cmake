macro(m_include_directories list)
    include_directories(${${list}})
endmacro(m_include_directories)

macro(m_copy_files m_from m_to)
    file(GLOB m_contents ${m_from}/*)
    file(COPY ${m_contents} DESTINATION ${m_to})
endmacro(m_copy_files)

macro(m_copy_files_post m_from m_to target)
    file(GLOB m_contents ${m_from}/*)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E
            make_directory ${m_to})
    foreach (DEPENDENCY ${m_contents})
        add_custom_command(TARGET ${target} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E
                copy_if_different ${DEPENDENCY} ${m_to})
    endforeach ()
endmacro(m_copy_files_post)

macro(m_find_library_or_build_once m_libname m_location m_cmake_source_loc m_target)
    find_library(SEARCHED_LIB_${m_target} NAMES ${m_libname} PATHS ${m_location})
    if (NOT SEARCHED_LIB_${m_target})
        message(WARNING "${m_libname} libary not found at ${m_location} - will build from source")
        add_subdirectory(${m_cmake_source_loc})
        set_target_properties(${m_target}
                PROPERTIES
                ARCHIVE_OUTPUT_DIRECTORY "${m_location}/archive"
                LIBRARY_OUTPUT_DIRECTORY "${m_location}/lib"
                RUNTIME_OUTPUT_DIRECTORY "${m_location}")
    else()
        message(STATUS "Found library at ${SEARCHED_LIB_${m_target}}")
    endif ()
endmacro(m_find_library_or_build_once)

macro(m_find_library_and_build m_libname m_location m_cmake_source_loc m_target)
    find_library(SEARCHED_LIB_${m_target} NAMES ${m_libname} PATHS ${m_location})
    if (NOT SEARCHED_LIB_${m_target})
        message(WARNING "${m_libname} libary not found at ${m_location} - will build from source")
    else ()
        message(STATUS "Found library at ${SEARCHED_LIB_${m_target}}")
    endif ()

    add_subdirectory(${m_cmake_source_loc})
    set_target_properties(${m_target}
            PROPERTIES
            ARCHIVE_OUTPUT_DIRECTORY "${m_location}/archive"
            LIBRARY_OUTPUT_DIRECTORY "${m_location}/lib"
            RUNTIME_OUTPUT_DIRECTORY "${m_location}")
endmacro(m_find_library_and_build)