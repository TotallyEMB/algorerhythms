#include <iostream>
#include <MemoryPool.h>
#include <classics.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <ctime>


#define TYPE_SHORTCUTS

#ifdef TYPE_SHORTCUTS
#ifndef ui32
#define ui32 unsigned
#endif
#ifndef i32
#define i32 int
#endif
#ifndef uc8
#define uc8 unsigned char
#endif
#ifndef c8
#define c8 char
#endif
#ifndef f32
#define f32 float
#endif
#ifndef f64
#define f64 double
#endif
#endif //TYPE_SHORTCUTS

struct Person {
    unsigned int id;
    unsigned int age;
};

void memory_pool(){
    MemoryPool memoryPool;

    void *allocOne = memoryPool.allocate();
    if (allocOne != (void *) 0) {
        Person person;
        person.id = 10230;
        person.age = 5;

        std::memcpy(allocOne, &person, sizeof(Person));

        Person *ptr = (Person *) allocOne;

        if (ptr->id == 10230 && ptr->age == 5) {
            std::cout << "correctly allocated\n";
        }

        memoryPool.free(allocOne);
        Person person2;
        person.id = 333;
        person.age = 24;

        void *allocTwo = memoryPool.allocate();
        std::memcpy(allocTwo, &person2, sizeof(Person));

        if (ptr->id != 10230 && ptr->age != 5) {
            std::cout << "correctly freed\n";
        }

        memoryPool.print();
    }
}

void test_adler32() {
    const uc8 str[] = "Test Strings";
    printf("string size %i \n", (int) sizeof(str));
    int hash = adler32(str, sizeof(str));
    printf("string hash #0x%0X %i\n", hash, hash);
}

void test_sort_quick() {
    unsigned int arrSize = 10000;
    float arr[arrSize];
    time_t t;
    time(&t);
    srand((unsigned) t);
    for (int i = 0; i < arrSize; i++) {
        arr[i] = (float) (rand() % 100);
    }
    clock_t startT = clock();
    quick_sort(arr, 0, sizeof(arr) / sizeof(float) - 1);
    printf("Time to sort %i floats in %f seconds\n", arrSize, ((float) (clock() - startT)) / CLOCKS_PER_SEC);
}

void test_search_binary() {
    unsigned int arrSize = 10000;
    float arr[arrSize];
    for (int i = 0; i < arrSize; i++) {
        arr[i] = (float) (i);
    }
    clock_t startT = clock();
    int result = binary_search(arr, 0, sizeof(arr) / sizeof(float) - 1, 8323);
    printf("Time to find position %i of a float in %f seconds\n", result, ((float) (clock() - startT)) / CLOCKS_PER_SEC);
}

void test_search_binary_iter() {
    unsigned int arrSize = 10000;
    float arr[arrSize];
    for (int i = 0; i < arrSize; i++) {
        arr[i] = (float) (i);
    }
    clock_t startT = clock();
    int result = binary_search_iter(arr, sizeof(arr) / sizeof(float) - 1, 8323);
    printf("Time to find position %i of a float in %f seconds\n", result, ((float) (clock() - startT)) / CLOCKS_PER_SEC);
}


#include <rendering.h>

int main(){
    memory_pool();
    test_adler32();
    test_sort_quick();
    test_search_binary_iter();
    test_search_binary();


    eb_render renderer;
    eb_render_init(&renderer);

    eb_render_model_settings settings;

    eb_render_handle handle = eb_render_create_model(&renderer, settings);

    eb_render_draw_all(&renderer);
    eb_render_draw_handle(&renderer, handle);
    eb_render_handle handleArr[] = {};
    eb_render_draw_handle_list(&renderer, handleArr, 3);



    return 0;
}
