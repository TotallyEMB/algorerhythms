cmake_minimum_required(VERSION 3.2)
project(AlGoreRhythms C CXX)

include_directories(allocator algorithms systems)

include(${CMAKE_SOURCE_DIR}/cmake/macros.cmake)

m_find_library_and_build(renderer ${PROJECT_SOURCE_DIR}/bin systems renderer)
m_find_library_and_build(mempool ${PROJECT_SOURCE_DIR}/bin allocator mempool)
m_find_library_and_build(classicalgor ${PROJECT_SOURCE_DIR}/bin algorithms classicalgor)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)

add_executable(Test main.cpp)
target_link_libraries(Test mempool classicalgor renderer)

