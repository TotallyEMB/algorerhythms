#ifndef RENDERING_H
#define RENDERING_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct eb_render {

} eb_render;

typedef struct eb_render_model_settings {

} eb_render_model_settings;

typedef struct eb_render_handle {

} eb_render_handle;

void eb_render_init(eb_render*);
eb_render_handle eb_render_create_model(eb_render *, eb_render_model_settings);
void eb_render_draw_all(eb_render *);
void eb_render_draw_handle(eb_render *, eb_render_handle);
void eb_render_draw_handle_list(eb_render *, eb_render_handle *arr, unsigned int num);


#ifdef __cplusplus
}
#endif

#endif